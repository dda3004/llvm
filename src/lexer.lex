%option noyywrap nounput noinput

%{
#include <cstdlib>
#include <iostream>
#include <string>
#include "ast.hpp"
#include "statement.hpp"

#include "parser.tab.hpp"

%}

%%

"var" return var_token;
"func" return fun_token;
"return" return return_token;
"if" return if_token;
"else" return else_token;
"println" return print_token;
"while" return while_token;
"do" return do_token;
"for" return for_token;
"==" return eq_token;
"!=" return ne_token;
"<=" return le_token;
">=" return ge_token;
"&&" return andl_token;
"||" return orl_token;
"!" return notl_token;

"int" return int_type_token; /* Migrate to actual types in the future? */
"string" return string_type_token;

[a-zA-Z_][a-zA-Z_0-9]* {
  yylval.string_value = new std::string(yytext);
  return id_token;
}

-?[0-9]+ {
  yylval.int_value = atoi(yytext);
  return int_token;
}

\".+?\" {
    // TODO remove quotemarks
    yylval.string_value = new std::string(yytext);
    return str_token;
}

[-=(),;%+*/<>{}\n:] return *yytext;

[ \t\r] {}

. {
  std::cerr << "Lexical error, unknown character: '" << *yytext << "'" << std::endl;
  exit(EXIT_FAILURE);
}

%%
